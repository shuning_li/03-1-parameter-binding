package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/api")
public class DateTimeController {
    @PostMapping("/datetimes")
    public DateTime getDateTime(@RequestBody DateTime datetime) {
        return datetime;
    }
}

class DateTime {
    private ZonedDateTime dateTime;

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
