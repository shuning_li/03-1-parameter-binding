package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserController {
    @GetMapping("/users/{id}")
     public String getUserId(@PathVariable Integer id) {
        return "" + id;
    }

    @GetMapping("/users_2/{id}")
    public String getUserIdByPrimitiveType(@PathVariable int id) {
        return "" + id;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getUserIdAndBookId(@PathVariable int userId, @PathVariable int bookId) {
        return "" + userId + " " + bookId;
    }

    @GetMapping("/users_3")
    public String getUserName(@RequestParam String name) {
        return name;
    }

    @GetMapping("/users_4")
    public String getUserAge(@RequestParam(defaultValue = "22") int age) {
        return "" + age;
    }

    @GetMapping("/users_5")
    public String getUserInfo(@RequestParam Map<String,String> userInfo) {
        return "name" + "=" + userInfo.get("name") + ";" + "age" + "=" + userInfo.get("age");
    }


}
