package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

@RestController
@RequestMapping("/api")
public class StudentController {
    @PostMapping("/student")
    public Student createStudent(@RequestBody @Valid Student student) {
        return student;
    }
}

class Student {
    @NotNull
    @Size(min = 2)
    private String name;

    @Max(9999)
    @Min(1000)
    private Integer yearOfBirth;

    @Email
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}