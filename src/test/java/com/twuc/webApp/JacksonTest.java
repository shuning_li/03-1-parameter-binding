package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class JacksonTest {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    void should_serialize_student() throws JsonProcessingException {
        String json = mapper.writeValueAsString(new Student("Shuning", 22));
        String expected = "{\"name\":\"Shuning\",\"age\":22}";
        Assertions.assertEquals(expected, json);
    }

    @Test
    void should_deserialize_student() throws IOException {
        String str = "{\"name\":\"Shuning\",\"age\":22}";
        Student student = mapper.readValue(str, Student.class);
        Assertions.assertEquals("Shuning", student.getName());
        Assertions.assertEquals(22, student.getAge());
    }
}

class Student {
    private String name;
    private int age;

    public Student() {}

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}