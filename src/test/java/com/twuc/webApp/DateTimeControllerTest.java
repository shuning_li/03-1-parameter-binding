package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DateTimeControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_datetime() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON).content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"dateTime\":\"2019-10-01T10:00:00Z\"}" ));
    }
}
