package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_when_params_is_correct() throws Exception {
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Shuning\",\"yearOfBirth\":1996,\"email\":\"1955142623@qq.com\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"name\":\"Shuning\",\"yearOfBirth\":1996,\"email\":\"1955142623@qq.com\"}"));
    }

    @Test
    void should_return_400_when_params_is_null() throws Exception {
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"yearOfBirth\":1996,\"email\":\"1955142623@qq.com\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_name_size_is_not_correct() throws Exception {
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"S\",\"yearOfBirth\":1996,\"email\":\"1955142623@qq.com\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_year_of_birth_is_not_within_the_range() throws Exception {
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Shuning\",\"yearOfBirth\":999,\"email\":\"1955142623@qq.com\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_email_is_not_correct() throws Exception {
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Shuning\",\"yearOfBirth\":999,\"email\":\"1955142623\"}"))
                .andExpect(status().isBadRequest());
    }
}
