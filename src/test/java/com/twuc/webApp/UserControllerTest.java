package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_user_id() throws Exception {
        mockMvc.perform(get("/api/users/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("123"));
    }

    @Test
    void should_get_user_id_when_using_primitive() throws Exception {
        mockMvc.perform(get("/api/users_2/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("123"));
    }

    @Test
    void should_get_user_id_and_book_id() throws  Exception {
        mockMvc.perform(get("/api/users/123/books/456"))
                .andExpect(status().isOk())
                .andExpect(content().string("123 456"));
    }

    @Test
    void should_get_user_name() throws Exception {
        mockMvc.perform(get("/api/users_3?name=Shuning"))
                .andExpect(status().isOk())
                .andExpect(content().string("Shuning"));
    }

    @Test
    void should_get_4xx_when_not_pass_params() throws Exception {
        mockMvc.perform(get("/api/users_3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_default_value() throws Exception {
        mockMvc.perform(get("/api/users_4"))
                .andExpect(status().isOk())
                .andExpect(content().string("22"));
    }

    @Test
    void should_get_user_info() throws Exception {
        mockMvc.perform(get("/api/users_5?name=Shuning&age=22"))
                .andExpect(status().isOk())
                .andExpect(content().string("name=Shuning;age=22"));
    }
}
